package com.rediff.createaccount;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.util.Common;

public class RediffCreateAccountTest extends Common {

	@BeforeMethod
	public void openBrowser() throws IOException {
		loadFiles();
		initBrowser();
	}

	@Test
	public void verifyCreateAccount() throws InterruptedException, IOException {
		SoftAssert softAssertion = new SoftAssert();
		try {
			driver.findElement(By.linkText(objects.getProperty("createAccountLinkText"))).click();

			driver.findElement(By.xpath(objects.getProperty("firstNameInputField"))).sendKeys("manidhar");
			driver.findElement(By.xpath(objects.getProperty("emailIputField"))).sendKeys("manidhar");

			driver.findElement(By.xpath(objects.getProperty("checkAvailabilityButton"))).click();
			Thread.sleep(1500);
			String actualError = driver.findElement(By.xpath(objects.getProperty("checkAvailabilityError"))).getText();
			String expectedError = "Sorry, the ID that you are looking for is taken.";

			System.out.println(actualError);
			System.out.println(expectedError);
			softAssertion.assertEquals(actualError, expectedError);
			softAssertion.assertAll();
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
			softAssertion.assertAll();
		}
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

}
