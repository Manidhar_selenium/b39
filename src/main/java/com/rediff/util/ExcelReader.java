package com.rediff.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelReader extends Common{

	public Object[][] readExcelData() throws EncryptedDocumentException, IOException{
		InputStream file=new FileInputStream(path+"/src/main/resources/TestData.xlsx");
		Workbook workbook=WorkbookFactory.create(file);
		Sheet sheet=workbook.getSheetAt(0);
		
		int lastRowNo=sheet.getLastRowNum();
		int totalRows=lastRowNo+1;
		System.out.println("totalRows: "+totalRows);
		
		Row row=sheet.getRow(0);
		int totalColumns=row.getLastCellNum();
		System.out.println("totalColumns: "+totalColumns);
		
		Object o[][]=new Object[totalRows][totalColumns];
		
		//rows
		for(int i=0;i<totalRows;i++) {
			row=sheet.getRow(i);
			totalColumns=row.getPhysicalNumberOfCells();
			//cells
			for(int j=0;j<2;j++) {
				Cell cell=row.getCell(j);
				o[i][j]=cell.getStringCellValue();
				//System.out.println(o[i][j]);
			}
			
		}
		return o;
	}
	
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		ExcelReader r=new ExcelReader();
		r.readExcelData();
	}

}
