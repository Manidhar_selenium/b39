package com.rediff.login;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.util.Common;
import com.rediff.util.ExcelReader;

public class RediffLoginTest extends Common {

	@BeforeMethod
	public void openBrowser() throws IOException {
		loadFiles();
		initBrowser();
	}

	@Test(priority = 1,enabled=false)
	public void verifyPageTitle() throws IOException {
		SoftAssert softAssertion = new SoftAssert();

		try {

			String expectedTitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
			String actualTitle = driver.getTitle();
			// Assert.assertEquals(actualTitle, expectedTitle);
			softAssertion.assertEquals(actualTitle, expectedTitle, "Page title not matched check the flow manually"); // Failed---Message1
			System.out.println("Validated Page title");
			softAssertion.assertEquals(actualTitle, expectedTitle, "Message2");// Failed
			softAssertion.assertEquals(actualTitle, expectedTitle, "Message3");// Failed---Message2

			softAssertion.assertAll();
		} catch (Exception e) {
			e.printStackTrace();

			captureScreenshot();
			softAssertion.assertAll();

		}
	}

	@Test(priority = 2, enabled = true, invocationCount = 1, dataProvider = "testData")
	public void invalidLoginTest(String emailId, String password) throws IOException {
		SoftAssert softAssertion = new SoftAssert();
		try {
			// 3. Click on 'Sign in' link
			driver.findElement(By.linkText(objects.getProperty("signInLinkText"))).click();
			// 4. Enter invalid user name as 'sdfsdfe@sdfd.com'
			driver.findElement(By.id(objects.getProperty("userNameInputField"))).sendKeys(emailId);
			// 5. Enter invalid password as 'sdfsf345fdg'
			driver.findElement(By.id(objects.getProperty("passwordInputField"))).sendKeys(password);
			// 6. Click on 'Sign in' button
			driver.findElement(By.name(objects.getProperty("submitButton"))).click();
			// 7. Validate the error message
			String actualErrorMessage = driver.findElement(By.id(objects.getProperty("errorMessageId"))).getText();
			String expectedErrorMessage = "Wrong username and password combination.";

			System.out.println("actualErrorMessage: " + actualErrorMessage);
			System.out.println("expectedErrorMessage: " + expectedErrorMessage);

			softAssertion.assertEquals(actualErrorMessage, expectedErrorMessage);
			softAssertion.assertAll();

		} catch (Exception e) {
			e.printStackTrace();

			captureScreenshot();
			softAssertion.assertAll();
		}

	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

	@DataProvider
	public Object[][] testData() throws EncryptedDocumentException, IOException {
		return new ExcelReader().readExcelData();
	}

}
